FROM node:8

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=production

COPY . .

COPY .env .

ENV port=4001
EXPOSE 4001

CMD [ "npm", "start" ]




