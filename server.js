'use strict';

require('dotenv').config();
const Hapi = require('hapi');
const mongoose = require('mongoose');
const glob = require('glob');
const path = require('path');
const User = require('./api/users/model/User');

const validate = async (decoded, request) => {
  try {
    const user = await User.findOne({ _id: decoded.sub }).exec();
    if (!user) {
      return {
        credentials: null,
        isValid: false
      };
    }

    return {
      credentials: decoded,
      isValid: true
    };
  } catch (error) {
    console.error(error);
    return {
      credentials: null,
      isValid: false
    };
  }
  if (!user) {
    return { credentials: null, isValid: false };
  }

  const isValid = await Bcrypt.compare(password, user.password);
  const credentials = { id: user.id, name: user.username };

  return { isValid, credentials };
};

// remote connection to database
const dbUrl = `mongodb://${process.env.MLAB_USER}:${
  process.env.MLAB_PASSWORD
}@${process.env.MLAB_DOMAIN}/${process.env.MLAB_DB}`;

(async () => {
  // The connection object takes some
  // configuration, including the port
  const server = new Hapi.Server({
    port: process.env.port || 3001,
    routes: {
      cors: {
        origin: [
          '*'
        ],
        additionalHeaders: [
          'Connection',
          'Pragma',
          'Cache-Control',
          'Access-Control-Request-Method',
          'Origin',
          'Access-Control-Request-Headers',
          'Accept',
          'Accept-Encoding',
          ]
      }
    }
  });

  await server.register(require('hapi-auth-jwt2'));

  // We are giving the strategy a name of 'jwt'
  server.auth.strategy('jwt', 'jwt', {
    validate,
    key: process.env.SECRET_KEY,
    verifyOptions: {
      algorithms: ['HS256']
    }
  });

  server.auth.default('jwt');

  // Look through the routes in
  // all the subdirectories of API
  // and create a new route for each
  glob.sync('api/**/routes/*.js', { root: __dirname }).forEach(file => {
    const route = require(path.join(__dirname, file));
    if (route.method && route.path) {
      server.route(route);
    }
  });

  // Start the server
  await server.start();
  console.info(`Server started at ${server.info.uri}`);

  // Once started, connect to Mongo through Mongoose
  try {
    const db = await mongoose.connect(
      dbUrl,
      { useNewUrlParser: true }
    );
    console.info(`Mongoose connected`);
  } catch (error) {
    throw err;
  }
})();
