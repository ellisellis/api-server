'use strict';

const User = require('../../users/model/User');
const Boom = require('boom');

module.exports = {
  method: 'GET',
  path: '/api/profile/{id}',
  config: {
    auth: false,
    handler: async req => {
      const _id = req.params.id;
      try {
        const user = await User.findOne({ _id })
          .select('-password -__v -admin -username')
          .exec();

        if (!user) {
          return Boom.notFound('No user found!');
        }

        const response = {
          completed: user.completed
        };

        return response;
      } catch (error) {
        if (error) {
          return Boom.badRequest(error);
        }
      }
    }
  }
};
