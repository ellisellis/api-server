const Submission = require('../model/Submission');

module.exports = {
  method: 'GET',
  path: '/api/submissions/team/{id}/{challenge_id}',
  config: {
    auth: {
      strategy: 'jwt',
      scope: ['player', 'admin']
    },
    handler: async req => {
      // const Key = `${team}/${payload.fileKey.hapi.filename}`;

      const submissions = await Submission.findOne({
        team_id: req.params.id,
        challenge_id: req.params.challenge_id
      })
        .select('-__v')
        .exec();

      return submissions;
    }
  }
};
