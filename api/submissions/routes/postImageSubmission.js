const Boom = require('boom');
require('dotenv').config();
const AWS = require('aws-sdk');
const { fireNotifications } = require('../../../util/notifications');
const { getOrCreateSubmission } = require('../../../util/submissionFunctions');

const DO_ACCESS_KEY = process.env.DO_ACCESS_KEY;
const DO_ACCESS_SECRET = process.env.DO_ACCESS_SECRET;
const DO_BUCKET = process.env.DO_BUCKET;

const spacesEndpoint = new AWS.Endpoint('nyc3.digitaloceanspaces.com');
const s3 = new AWS.S3({
  endpoint: spacesEndpoint,
  accessKeyId: DO_ACCESS_KEY,
  secretAccessKey: DO_ACCESS_SECRET
});

const suffixRegex = /.*\.(jpe?g|gif|png|tiff)$/i;

module.exports = {
  method: 'POST',
  path: '/api/submissions/image/{challenge_id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    payload: {
      output: 'stream',
      allow: 'multipart/form-data',
      maxBytes: (1024 * 1024) * 10
    },
    handler: async req => {
      const { sub: team_id, username: team_name } = req.auth.credentials;

      const { challenge_id } = req.params;

      const payload = req.payload;
      const matches = suffixRegex.exec(payload.fileKey.hapi.filename);
      const suffix = matches.slice(-1).pop();

      const Key = `${team_name}/${Date.now()}.${suffix}`;

      const fileUpload = await new Promise(async (resolve, reject) => {
        const config = {
          Body: payload.fileKey._data,
          Bucket: DO_BUCKET,
          Key,
          ACL: 'public-read',
          ContentType: `image/${suffix}`
        };

        s3.putObject(config, err => {
          if (err) {
            console.error(err);
            reject(Boom.badData(err));
          } else {
            resolve(true);
          }
        });
      });

      if (fileUpload) {
        const submission = await getOrCreateSubmission(
          challenge_id,
          team_id,
          team_name
        );

        const attempt = {
          imageUrl: Key
        };

        submission.attempts.push(attempt);
        submission.status = 'pending';
        try {
          await submission.save();
          try {
            await fireNotifications(submission);
          } catch (error) {
            console.error(error);
          }
          return {
            message: 'Submission Created',
            body: submission
          };
        } catch (error) {
          console.error(error);
          return Boom.badData(error);
        }
      }
    }
  }
};
