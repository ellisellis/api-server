const Boom = require('boom');
require('dotenv').config();
const { fireNotifications } = require('../../../util/notifications');
const { getOrCreateSubmission } = require('../../../util/submissionFunctions');

module.exports = {
  method: 'POST',
  path: '/api/submissions/text/{challenge_id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    handler: async req => {
      const { sub: team_id, username: team } = req.auth.credentials;
      const { challenge_id } = req.params;

      const err = req.payload.error;
      if (err) {
        console.error(err);
        return;
      }
      const text = req.payload.text;

      const submission = await getOrCreateSubmission(
        challenge_id,
        team_id,
        team
      );
      const attempt = { text };

      submission.attempts.push(attempt);
      submission.status = 'pending';

      try {
        await submission.save();
        try {
          await fireNotifications(submission);
        } catch (error) {
          if (err) {
            return Boom.badRequest(err);
          }
        }
        return {
          message: 'Submission Created',
          body: submission
        };
      } catch (e) {
        return Boom.badRequest(e);
      }
    }
  }
};
