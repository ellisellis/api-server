const Boom = require('boom');
require('dotenv').config();
const AWS = require('aws-sdk');
const Submission = require('../model/Submission');

const DO_ACCESS_KEY = process.env.DO_ACCESS_KEY;
const DO_ACCESS_SECRET = process.env.DO_ACCESS_SECRET;
const DO_BUCKET = process.env.DO_BUCKET;

const spacesEndpoint = new AWS.Endpoint('nyc3.digitaloceanspaces.com');
const s3 = new AWS.S3({
  endpoint: spacesEndpoint,
  accessKeyId: DO_ACCESS_KEY,
  secretAccessKey: DO_ACCESS_SECRET
});

module.exports = {
  method: 'GET',
  path: '/api/submissions/{sub_id}/image/{attempt_id}',
  config: {
    auth: {
      strategy: 'jwt',
      scope: ['admin']
    },
    handler: async req => {

      const sub_id = req.params.sub_id;
      const attempt_id = req.params.attempt_id;

      const submission = await Submission.findOne({_id: sub_id})
      .select('-__v')
      .exec();

      const attempt = submission.attempts.id(attempt_id);

      try {
        const data = await fetchImageFromS3(attempt.imageUrl);
        return data;
      } catch (error) {
        return Boom.badRequest(error);
      }

    }
  }
};

async function fetchImageFromS3(Key) {
  const params = {
    Key,
    Bucket: DO_BUCKET
  };
  return new Promise((resolve, reject) => {
    s3.getObject(params, (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data.Body);
    });
  });
}
