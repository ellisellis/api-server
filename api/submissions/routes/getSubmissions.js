const Submission = require('../model/Submission');

module.exports = {
  method: 'GET',
  path: '/api/submissions',
  config: {
    auth: {
      strategy: 'jwt'
    },
    handler: async req => {
      // SEND ALL THE THINGS TO THE GLORIOUS ADMIN
      if (req.auth.credentials.role === 'admin') {
        return await Submission.find()
          .select('-__v')
          .exec();
      }

      // send only the team's submissions
      const submissions = await Submission.find({
        team_name: req.auth.credentials.username
      })
        .select('-__v')
        .exec();

      return submissions;
    }
  }
};
