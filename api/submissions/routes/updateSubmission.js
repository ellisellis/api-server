const Submission = require('../model/Submission');
const User = require('../../users/model/User');
const Boom = require('boom');

module.exports = {
  method: 'PATCH',
  path: '/api/submissions/{id}',
  config: {
    auth: {
      strategy: 'jwt',
      scope: ['admin']
    },
    handler: async req => {
      const _id = req.params.id;

      const submission = await Submission.findOne({ _id }).exec();

      submission.status = req.payload.status;
      submission.messages = [...submission.messages, {text:req.payload.message}];

      try {
        await submission.save();
      } catch (error) {
        return Boom.internal(error);
      }

      if (submission.status === 'completed') {
        const user = await User.findOne({ _id: submission.team_id }).exec();
        const completed = [...user.completed, submission.challenge_id];
        user.completed = completed;
        await user.save();
      }

      return {
        message: 'successfully updated',
        submission,
      };
    }
  }
};
