const Boom = require('boom');
require('dotenv').config();
const Submission = require('../model/Submission');

module.exports = {
  method: 'GET',
  path: '/api/submissions/{id}',
  config: {
    auth: {
      strategy: 'jwt',
      scope: ['admin']
    },
    handler: async req => {

      const id = req.params.id;

      const submission = await Submission.findOne({_id: id})
      .select('-__v')
      .exec();

      return submission;
    }
  }
};
