'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const message = new Schema(
  {
    text: String
  },
  { timestamps: true }
);

module.exports = message;
