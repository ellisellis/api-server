'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const attemptModel = new Schema(
  {
    text: String,
    imageUrl: String
  },
  { timestamps: true }
);

module.exports = attemptModel;
