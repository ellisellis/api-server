'use strict';

const mongoose = require('mongoose');
const Attempt = require('./Attempt');
const Message = require('./Message');
const Schema = mongoose.Schema;

const submissionModel = new Schema({
  team_id: { type: String, required: true },
  challenge_id: { type: String, required: true },
  team_name: { type: String, required: false },
  type: String,
  status: { type: String, required: false },
  messages: [{ type: Message, required: false, default: [] }],
  attempts: [{ type: Attempt, required: false, default: [] }]
});

module.exports = mongoose.model('Submission', submissionModel);
