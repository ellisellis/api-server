'use strict';

const Boom = require('boom');
const Challenge = require('../model/Challenge');
const postChallengeSchema = require('../schemas/postChallenge');

module.exports = {
  method: 'POST',
  path: '/api/challenges',
  config: {
    auth: {
      strategy: 'jwt',
      scope: ['admin']
    },
    payload: {
      allow: ['application/json', 'multipart/form-data', 'image/jpeg', 'application/pdf', 'application/x-www-form-urlencoded']
    },
    log: {
      collect: true
    },
    handler: async req => {
      let challenge = new Challenge(req.payload.value);
      try {
        const data = await challenge.save();
        return { message: 'Challenge created!', data };
      } catch (error) {
        return Boom.badRequest(error);
      }
    },
    // Validate the payload against the Joi schema
    validate: {
      payload: postChallengeSchema
    }
  }
};
