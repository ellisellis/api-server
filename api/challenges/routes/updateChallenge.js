'use strict';

const Boom = require('boom');
const Challenge = require('../model/Challenge');
const updateChallengeSchema = require('../schemas/updateChallenge');

module.exports = {
  method: 'PATCH',
  path: '/api/challenges/{id}',
  config: {
    auth: {
      strategy: 'jwt',
      scope: ['admin']
    },
    handler: async req => {
      const id = req.params.value.id;
      try {
        const challenge = await Challenge.findOneAndUpdate(
          { _id: id },
          req.payload.value
        );
        if (!challenge) {
          return Boom.notFound('Challenge not found!');
        }
        return { message: 'Challenge updated!' };
      } catch (error) {
        return Boom.badRequest(error);
      }
    },
    validate: {
      payload: updateChallengeSchema.payloadSchema,
      params: updateChallengeSchema.paramsSchema
    }
  }
};
