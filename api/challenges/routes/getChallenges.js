'use strict';

const Challenge = require('../model/Challenge');
const Boom = require('boom');

module.exports = {
  method: 'GET',
  path: '/api/challenges',
  config: {
    auth: false,
    handler: async () => {
      try {
        const data = await Challenge.find()
          // Deselect the password and version fields
          .select('-__v')
          .exec();
        if (!data.length) {
          return Boom.notFound('No challenges found!');
        }
        return data;
      } catch (error) {
        return Boom.badRequest(err);
      }
    }
  }
};
