'use strict';

const Challenge = require('../model/Challenge');
const Boom = require('boom');

module.exports = {
  method: 'GET',
  path: '/api/challenges/{id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    handler: async (req) => {
      const _id = req.params.id;

      try {
        const challenge = await Challenge.findOne({ _id })
          // Deselect the password and version fields
          .select('-__v')
          .exec();
        if (!challenge) {
          return Boom.notFound('Challenge not found!');
        }
        return challenge;
      } catch (error) {
        return Boom.badRequest(error);
      }
    }
  }
};
