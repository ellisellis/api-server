"use strict";

const Boom = require("boom");
const Challenge = require("../model/Challenge");
const deleteChallengeSchema = require("../schemas/deleteChallenge");

module.exports = {
  method: "DELETE",
  path: "/api/challenges/{id}",
  config: {
    auth: {
      strategy: "jwt",
      scope: ["admin"]
    },
    handler: async req => {
      const _id = req.params.value.id;

      const result = await Challenge.findOneAndRemove({ _id }).exec();

      if (!result) {
        console.error(result);
        return Boom.badRequest('Failed to delete challenge');
      }
      return { message: "Challenge deleted!" };
    },
    // Validate the payload against the Joi schema
    validate: {
      params: deleteChallengeSchema
    }
  }
};
