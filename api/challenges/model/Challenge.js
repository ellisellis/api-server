"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const challengeModel = new Schema({
  short_name: { type: String, required: true },
  description: { type: String, required: true },
  specific: { type: String },
  physical_map_id: { type: String},
  point_value: { type: String},
  location_type: { type: String},
  answer: { type: String},
  notes: { type: String},
  type: { type: String}
});

module.exports = mongoose.model("Challenge", challengeModel);
