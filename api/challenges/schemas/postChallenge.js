'use strict';

const Joi = require('joi');

const postChallengeSchema = Joi.object({
  short_name: Joi.string().required(),
  description: Joi.string().required(),
  type: Joi.string()
    .allow('image', 'text')
    .required(),
  physical_map_id: Joi.number(),
  point_value: Joi.number().required(),
  specific: Joi.string(),
  location_type: Joi.string(),
  answer: Joi.string(),
  notes: Joi.string()
});

module.exports = postChallengeSchema;
