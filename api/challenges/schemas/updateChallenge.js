'use strict';

const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const payloadSchema = Joi.object({
  short_name: Joi.string(),
  description: Joi.string(),
  type: Joi.string().allow('image', 'text'),
  physical_map_id: Joi.number(),
  point_value: Joi.number().required(),
  specific: Joi.string(),
  location_type: Joi.string(),
  answer: Joi.string(),
  notes: Joi.string()
});

const paramsSchema = Joi.object({
  id: Joi.objectId().required()
});

module.exports = {
  payloadSchema: payloadSchema,
  paramsSchema: paramsSchema
};
