'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const subscription = new Schema({
  user_id: String,
  endpoint: { type: String, required: true },
  expirationTime: Number,
  keys: {
    auth: String,
    p256dh: String
  }
});

module.exports = mongoose.model('Subscription', subscription);
