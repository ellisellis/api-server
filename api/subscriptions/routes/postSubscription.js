const Boom = require('boom');
const Subscription = require('../models/Subscription');
const postSubscriptionSchema = require('../schema/postSubscription');

module.exports = {
  method: 'POST',
  path: '/api/subscriptions',
  config: {
    auth: {
      strategy: 'jwt',
      scope: ['player', 'admin']
    },
    handler: async req => {
      const payload = req.payload.value;
      const creds = req.auth.credentials;

      // check if the user's id is already in the subscriptions
      // const userExists = await Subscription.findOne({
      //   user_id: creds.sub
      // }).exec();
      // if (userExists) {
      //   return {
      //     message: 'user exists, skipping subscription'
      //   };
      // }

      try {
        const sub = new Subscription({
          ...payload,
          status: 'pending',
          user_id: creds.sub
        });
        await sub.save();
        return {
          message: sub
        };
      } catch (err) {
        return Boom.badRequest(err);
      }
    },
    validate: {
      payload: postSubscriptionSchema
    }
  }
};
