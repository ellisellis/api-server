'use strict';

const Joi = require('joi');

const postSubscriptionSchema = Joi.object({
  endpoint: Joi.string().required(),
  expirationTime: Joi.number().default(null),
  keys: Joi.object().keys({
    auth: Joi.string(),
    p256dh: Joi.string()
  })
});

module.exports = postSubscriptionSchema;
