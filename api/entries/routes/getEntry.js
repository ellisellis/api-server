"use strict";

const Entry = require("../model/Entry");
const Boom = require("boom");

module.exports = {
  method: "GET",
  path: "/api/entries/{id}",
  config: {
    auth: {
      strategy: "jwt"
    },
    handler: (req, res) => {
      const _id = req.params.value.id;

      Entry.findOne({ _id })
        // Deselect the password and version fields
        .select("-__v")
        .exec((err, data) => {
          if (err) {
            return Boom.badRequest(err);
          }
          if (!data) {
            return Boom.notFound("Entry not found!");
          }
          return data;
        });
    }
  }
};
