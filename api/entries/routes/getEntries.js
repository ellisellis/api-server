'use strict';

const Entry = require('../model/Entry');
const Boom = require('boom');

module.exports = {
  method: 'GET',
  path: '/api/entries',
  config: {
    auth: {
      strategy: 'jwt'
    },
    handler: (req, res) => {
      return (
        Entry.find()
          // Deselect the password and version fields
          .select('-__v')
          .exec()
      );
    }
  }
};
