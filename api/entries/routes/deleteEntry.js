'use strict';

const Boom = require('boom');
const Entry = require('../model/Entry');
const deleteEntrySchema = require('../schemas/deleteEntry');

module.exports = {
  method: 'DELETE',
  path: '/api/entries/{id}',
  config: {
    auth: {
      strategy: 'jwt',
      scope: ['admin']
    },
    handler: async (req, res) => {
      const _id = req.params.value.id;

      try {
        const entry = await Entry.findOneAndRemove({ _id }).exec();
        if (!entry) {
          return Boom.notFound('Entry not found!');
        }
        return { message: 'Entry deleted!' };
      } catch (error) {
        return Boom.badRequest(error);
      }
    },
    // Validate the payload against the Joi schema
    validate: {
      params: deleteEntrySchema
    }
  }
};
