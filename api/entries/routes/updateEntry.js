'use strict';

const Boom = require('boom');
const Entry = require('../model/Entry');
const updateEntrySchema = require('../schemas/updateEntry');
const verifyUniqueEntry = require('../../../util/entryFunctions')
  .verifyUniqueEntry;

module.exports = {
  method: 'PATCH',
  path: '/api/entries/{id}',
  config: {
    pre: [{ method: verifyUniqueEntry, assign: 'entry' }],
    auth: {
      strategy: 'jwt',
      scope: ['admin']
    },
    handler: async (req, res) => {
      const id = req.params.value.id;
      try {
        const entry = await Entry.findOneAndUpdate({ _id: id }, req.pre.entry);
        if (!entry) {
          return Boom.notFound('Entry not found!');
        }
        return { message: 'Entry updated!' };
      } catch (err) {
        if (err) {
          return Boom.badRequest(err);
        }
      }
    },
    validate: {
      payload: updateEntrySchema.payloadSchema,
      params: updateEntrySchema.paramsSchema
    }
  }
};
