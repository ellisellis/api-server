'use strict';

const Entry = require('../model/Entry');
const User = require('../../users/model/User');
const Challenge = require('../../challenges/model/Challenge');
const Boom = require('boom');

module.exports = {
  method: 'GET',
  path: '/api/entries/team/{team_id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    handler: async (req, res) => {
      const team_id = req.params.team_id;
      const user = await User.findOne({ _id: team_id }).exec();

      if (!user.completed) {
        return {
          message: 'no entries',
          entries: []
        };
      }

      try {
        const challenges = await Challenge.find({
          _id: {
            $in: user.completed
          }
        }).exec();

        const totalPoints = challenges.reduce(
          (acc, challenge) => acc + parseInt(challenge.point_value, 10),
          0
        );

        try {
          const entries = await Entry.find({
            threshold: {
              $lte: totalPoints
            }
          }).exec();

          return {
            message: 'ok',
            entries
          };
        } catch (error) {

        }

      } catch (error) {
        return {
          message: error,
          entries: []
        };
      }
    }
  }
};
