'use strict';

const Boom = require('boom');
const Entry = require('../model/Entry');
const postEntrySchema = require('../schemas/postEntry');

module.exports = {
  method: 'POST',
  path: '/api/entries',
  config: {
    auth: {
      strategy: 'jwt',
      scope: ['admin']
    },
    handler: async req => {
      let entry = new Entry(req.payload.value);

      try {
        const data = await entry.save();
        return { message: 'Entry created!', data };
      } catch (err) {
        return Boom.badRequest(err);
      }
    },
    // Validate the payload against the Joi schema
    validate: {
      payload: postEntrySchema
    }
  }
};
