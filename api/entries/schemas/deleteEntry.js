"use strict";

const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

const deleteEntrySchema = Joi.object({
  id: Joi.objectId().required()
});

module.exports = deleteEntrySchema;
