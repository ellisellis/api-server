"use strict";

const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

const payloadSchema = Joi.object({
  text: Joi.string(),
  date: Joi.string()
});

const paramsSchema = Joi.object({
  id: Joi.objectId().required()
});

module.exports = {
  payloadSchema: payloadSchema,
  paramsSchema: paramsSchema
};
