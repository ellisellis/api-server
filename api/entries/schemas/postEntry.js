"use strict";

const Joi = require("joi");

const postEntrySchema = Joi.object({
  date: Joi.string().required(),
  text: Joi.string().required()
});

module.exports = postEntrySchema;
