'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const entryModel = new Schema({
  date: { type: String, required: true },
  text: { type: String, required: true },
  threshold: { type: Number, required: true }
});

module.exports = mongoose.model('Entry', entryModel);
