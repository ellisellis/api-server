"use strict";

const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

const createUserSchema = Joi.object({
  username: Joi.string()
    .alphanum()
    .min(2)
    .max(30)
    .required(),
  password: Joi.string().required(),
  admin: Joi.boolean(),
  completed: Joi.array().items(Joi.objectId()).required()
});

module.exports = createUserSchema;
