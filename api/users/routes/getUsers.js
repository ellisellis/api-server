'use strict';

const User = require('../model/User');
const Boom = require('boom');

module.exports = {
  method: 'GET',
  path: '/api/users',
  config: {
    auth: {
      strategy: 'jwt',
      scope: ['admin']
    },
    handler: async req => {
      try {
        const users = await User.find()
          // Deselect the password and version fields
          .select('-password -__v')
          .exec();

        if (!users.length) {
          return Boom.notFound('No users found!');
        }
        return users;
      } catch (error) {
        return Boom.badRequest(error);
      }
    }
  }
};
