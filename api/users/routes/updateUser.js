"use strict";

const Boom = require("boom");
const User = require("../model/User");
const updateUserSchema = require("../schemas/updateUser");
const verifyUniqueUser = require("../../../util/userFunctions")
  .verifyUniqueUser;

module.exports = {
  method: "PATCH",
  path: "/api/users/{id}",
  config: {
    pre: [{ method: verifyUniqueUser, assign: "user" }],
    auth: {
      strategy: "jwt",
      scope: ["admin"]
    },
    handler: async (req) => {

      const params = req.params.value;
      const payload = req.payload.value;

      const id = params.id;

      const updateObj = {
        ...req.pre.user,
        ...payload
      };
      try {
        const user = await User.findOneAndUpdate({ _id: id }, updateObj);

        if (!user) {
          return Boom.notFound("User not found!");
        }
        return {
          message: "User updated!"
        };

      } catch (err) {
        return Boom.badRequest(err);
      }
    },
    validate: {
      payload: updateUserSchema.payloadSchema,
      params: updateUserSchema.paramsSchema
    }
  }
};
