'use strict';

const bcrypt = require('bcrypt');
const Boom = require('boom');
const User = require('../model/User');
const postUserSchema = require('../schemas/postUser');
const verifyUniqueUser = require('../../../util/userFunctions')
  .verifyUniqueUser;
const { createAdminToken } = require('../../../util/token');

async function hashPassword(password, cb) {
  // Generate a salt at level 10 strength
  const salt = await bcrypt.genSalt(10);

  const hash = await bcrypt.hash(password, salt);

  return hash;
}

module.exports = {
  method: 'POST',
  path: '/api/users',
  config: {
    auth: false,
    // Before the route handler runs, verify that the user is unique
    pre: [{ method: verifyUniqueUser }],
    handler: async (req, res) => {
      const payload = req.payload.value;

      let user = new User();
      user.username = payload.username;
      user.admin = payload.admin || false;

      if ( payload.admin ) {
        const hash = await hashPassword(payload.password);
        user.password = hash;
      } else {
        user.password = payload.password;
      }

      try {
        await user.save();
        return { token: createAdminToken(user) };
      } catch ( e ) {
        return Boom.badRequest(e)
      }
      // If the user is saved successfully, issue a JWT
    },
    // Validate the payload against the Joi schema
    validate: {
      payload: postUserSchema
    }
  }
};
