'use strict';

const Boom = require('boom');
const Subscription = require('../../subscriptions/models/Subscription');
const { fireNotifications } = require('../../../util/notifications');

module.exports = {
  method: 'POST',
  path: '/api/notifications',
  config: {
    auth: false,
    handler: async (req, res) => {
      try {
        await fireNotifications({ team_name: 'admin' });
        return {
          success: true
        };
      } catch (e) {
        return Boom.badRequest(e);
      }
    }
  }
};
