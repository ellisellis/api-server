'use strict';

const secret = process.env.SECRET_KEY;
const jwt = require('jsonwebtoken');

function createAdminToken(user) {
  // Sign the JWT
  try {
    return jwt.sign(
      {
        sub: user.id,
        username: user.username,
        role: user.admin ? 'admin' : 'player',
        scope: user.admin ? 'admin' : 'player'
      },
      secret,
      {
        algorithm: 'HS256',
        expiresIn: '20d'
      }
    );
  } catch (e) {
    throw Error('Bad JWT');
  }
}

module.exports = { createAdminToken };
