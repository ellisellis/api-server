'use strict';

const Boom = require('boom');
const User = require('../api/users/model/User');
const bcrypt = require('bcrypt');

async function verifyUniqueUser(req, res) {
  // Find an entry from the database that
  // matches either the email or username

  const payload = req.payload.value;

  const conditions = {
    password: payload.password
  };
  const user = await User.findOne(conditions).exec();

  // // Check whether the username or email
  // // is already taken and error out if so
  // if (user) {
  //   if (user.username === payload.username) {
  //     return Boom.badRequest('Username taken');
  //   }
  // }
  // If everything checks out, send the payload through
  // to the route handler
  return req.payload;
}

async function verifyCredentials(req, h, err) {
  const payload = req.payload.value;

  const username = payload.username;
  const password = payload.password;

  if (!username) {
    try {
      const password = payload.password;
      return await User.findOne({ password });
    } catch (error) {
      return Boom.serverUnavailable(error);
    }
  }

  // Find an entry from the database that
  // matches either the email or username
  try {
    const user = await User.findOne({ username }).exec();
    if (user) {
      const matches = await bcrypt.compare(password, user.password);
      if (matches) {
        return user;
      } else {
        return Boom.unauthorized("username and password don't match");
      }
    } else {
      return Boom.badRequest('No user found!');
    }
  } catch (e) {
    return Boom.serverUnavailable('Something went wrong');
  }
}

module.exports = {
  verifyUniqueUser: verifyUniqueUser,
  verifyCredentials: verifyCredentials
};
