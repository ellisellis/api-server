const Submission = require('../api/submissions/model/Submission');

async function getOrCreateSubmission(challenge_id, team_id, team_name) {
  const existing = await Submission.findOne({
    challenge_id,
    team_id
  }).exec();
  if (existing) {
    existing.attempts = existing.attempts || [];
    return existing;
  } else {
    return new Submission({
      challenge_id,
      team_id,
      team_name,
      messages: [],
      status: 'pending'
    });
  }
}
module.exports = {
  getOrCreateSubmission
};
