const Subscription = require('../api/subscriptions/models/Subscription');
const webpush = require('web-push');

require('dotenv').config();
const VAPID_MAILTO = process.env.VAPID_MAILTO;
const VAPID_PUBLIC_KEY = process.env.VAPID_PUBLIC_KEY;
const VAPID_PRIVATE_KEY = process.env.VAPID_PRIVATE_KEY;

webpush.setVapidDetails(VAPID_MAILTO, VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY);

function generateNotification(submission) {
  const payload = {
    notification: {
      title: `Team ${submission.team_name} has submitted a guess`,
      body: submission.text,
      vibrate: [100, 50, 100],
      data: {
        dateOfArrival: Date.now(),
        primaryKey: 1
      },
      actions: [
        {
          action: 'explore',
          title: "check user's input"
        }
      ]
    }
  };
  return payload;
}
async function fireNotifications(submission) {
  if ( !process.env.ENABLE_SERVICE_WORKERS ) {
    return Promise.resolve();
  }
  const payload = generateNotification(submission);
  const subs = await Subscription.find().exec();
  return await Promise.all(
    subs.map(sub => webpush.sendNotification(sub, JSON.stringify(payload)))
  );
}

module.exports = {
  fireNotifications,
  generateNotification
};
