'use strict';

const Boom = require('boom');
const Entry = require('../api/entries/model/Entry');
async function verifyUniqueEntry(req) {
  const payload = req.payload.value;
  // Find an entry from the database that
  // matches either the email or entryname
  const conditions = {
    $and: [{ date: payload.date }, { text: payload.text }]
  };
  const entry = await Entry.findOne(conditions).exec();
  // Check whether the entryname or email
  // is already taken and error out if so
  if (entry) {
    if (entry.date === payload.date) {
      return Boom.badRequest('Entry date taken');
    }
    if (entry.text === payload.text) {
      return Boom.badRequest('Entry text taken');
    }
  }
  // If everything checks out, send the payload through
  // to the route handler
  return payload;
}

module.exports = {
  verifyUniqueEntry: verifyUniqueEntry
};
